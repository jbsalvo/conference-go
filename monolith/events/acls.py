import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    headers = {
        "Authorization": PEXELS_API_KEY,
    }
    params = {"query": f"{city} {state}", "per_page": 1}
    res = requests.get(url, params=params, headers=headers)
    pexel_dict = res.json()

    # anti-corruption
    pic_url = pexel_dict["photos"][0]["src"]["original"]
    return {"picture_url": pic_url}


def get_geo_data(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": f"{city}, {state}, USA", "appid": OPEN_WEATHER_API_KEY}
    res = requests.get(url, params=params)
    geo_dict = res.json()
    lat = geo_dict[0]["lat"]
    lon = geo_dict[0]["lon"]
    return lat, lon


def get_weather(city, state):
    lat, lon = get_geo_data(city, state)
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lon": lon,
        "lat": lat,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    res = requests.get(url, params=params)
    weather_dict = res.json()
    return {
        "description": weather_dict["weather"][0]["description"],
        "temp": weather_dict["main"]["temp"],
    }
